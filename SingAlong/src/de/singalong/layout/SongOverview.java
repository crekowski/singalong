package de.singalong.layout;

import java.util.List;
import java.util.Set;

import de.singalong.R;
import de.singalong.data.Archive;
import de.singalong.data.Database;
import de.singalong.data.User;
import de.singalong.utils.Keys;
import de.singalong.utils.MessageUtils;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnShowListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

public class SongOverview extends MyActionBarActivity {
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_song_overview);
        
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.flSongOverview, new SongOverviewFragment())
                    .commit();
        }
    }
	
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.song_overview, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_invite) {
        	addUser();
            return true;
        }
        if (id == R.id.action_delete) {
        	removeArchive();
        	return true;
        }
        
        return super.onOptionsItemSelected(item);
    }
    
    public void addUser() {
		AddUserDialogFragment dialog;
		
		dialog = new AddUserDialogFragment();
		dialog.show(getSupportFragmentManager(), "AddUserDialogFragment");
	}
    
    public void removeArchive() {
    	RemoveArchiveDialogFragment dialog;
    	
    	dialog = new RemoveArchiveDialogFragment();
    	dialog.show(getSupportFragmentManager(), "RemoveArchiveDialogFragment");
    }

    
    public static class SongOverviewFragment extends Fragment {

    	private ArrayAdapter<String> songListAdapter;
    	private Archive              archive;
    	
        @Override
        public View onCreateView(
        		LayoutInflater inflater, 
        		ViewGroup container,
                Bundle savedInstanceState) 
        {
            View rootView = inflater.inflate(
            		R.layout.f_song_overview, container, false);
            
            createOverview(rootView);
            createListeners(rootView);
            
            return rootView;
        }
        
        private void createOverview(View view) {
        	Intent             intent;
        	final String       userName;
			final String       archiveName;
			final List<String> songOverview;
			
			intent       = getActivity().getIntent();
			userName     = intent.getStringExtra(Keys.USERNAME);
			archiveName  = intent.getStringExtra(Keys.ARCHIVENAME);
			archive      = Database.getArchive(userName, archiveName);
			songOverview = archive.getAllSongNames();
			
			getActivity().getActionBar().setTitle(archive.getName());
        	
			songListAdapter = new ArrayAdapter<String>(
					getActivity(),
    				android.R.layout.simple_list_item_1, 
    				songOverview);

    		// Assign adapter to ListView
    		ListView songList = (ListView) view.findViewById(R.id.lvSongs);
    		songList.setAdapter(songListAdapter);

    		// ListView Item Click Listener
    		songList.setOnItemClickListener(new OnItemClickListener() {

    			@Override
    			public void onItemClick(
    					AdapterView<?> parent, View view, int position, long id) 
    			{
    				Intent intent = new Intent(getActivity(), SongView.class);

    				intent.putExtra(Keys.USERNAME, userName);
    				intent.putExtra(Keys.ARCHIVENAME, archiveName);
    				intent.putExtra(Keys.SONGNAME, songOverview.get(position));
    				
    				startActivity(intent);
    			}

    		});
    	}
        
        private void createListeners(View view) {
			Button newSong;
			Button importSong;
			
			newSong    = (Button) view.findViewById(R.id.bNewSong);
			importSong = (Button) view.findViewById(R.id.bImportSong);

			newSong.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					NewSongDialogFragment dialog;
					
					dialog = new NewSongDialogFragment();
					dialog.show(
							getActivity().getSupportFragmentManager(), 
							"NewSongDialogFragment");
				}
			});
			
			importSong.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent currentIntent;
					Intent importIntent;
					String userName;
					String archiveName;
					
					currentIntent = getActivity().getIntent();
					importIntent  = new Intent(getActivity(), ImportSong.class);
					userName      = currentIntent.getStringExtra(Keys.USERNAME);
					archiveName   = 
							currentIntent.getStringExtra(Keys.ARCHIVENAME);
					
					importIntent.putExtra(Keys.USERNAME, userName);
					importIntent.putExtra(Keys.ARCHIVENAME, archiveName);
					
					startActivity(importIntent);
				}
			});
		}
        
        @Override
        public void onResume() {
        	super.onResume();
        	
        	refresh();
        }
        
		public void refresh() {
			// update archive list
			songListAdapter.clear();
			songListAdapter.addAll(archive.getAllSongNames());
		}
    }
    
    public static class AddUserDialogFragment extends DialogFragment {
    	
    	private EditText etAddUser;
    	private TextView tvAddUserMessage;
    	private Button   bAddUser;
    	
    	@Override
    	public Dialog onCreateDialog(Bundle savedInstancesState) {
            AlertDialog.Builder builder;
            LayoutInflater      inflater;
            final View          dialogView;
            final AlertDialog   dialog;
            
            builder          = new AlertDialog.Builder(getActivity());
            inflater         = getActivity().getLayoutInflater();
            dialogView       = 
            		inflater.inflate(R.layout.d_add_user_dialog, null);
            etAddUser        = (EditText) 
            		dialogView.findViewById(R.id.etAddUser);
            tvAddUserMessage = (TextView) 
            		dialogView.findViewById(R.id.tvAddUserMessage);
            
            builder.setView(dialogView)
            	   .setTitle(R.string.add_user_dialog_title)
                   .setPositiveButton(R.string.add, 
                		   new DialogInterface.OnClickListener() {
		                       public void onClick(
		                    		   DialogInterface dialog, int id) 
		                       {
		                    	   String userName;
		                    	   
		                    	   userName = etAddUser.getText().toString();
		                    	   
		                    	   notifyUser(userName);
		                    	   
		                    	   dialog.dismiss();
		                       }
                   		   })
                   .setNegativeButton(R.string.cancel, 
                		   new DialogInterface.OnClickListener() {
		                       public void onClick(
		                    		   DialogInterface dialog, int id) 
		                       {
		                           dialog.cancel();
		                       }
                   		   });
            
            dialog   = builder.create();
            bAddUser = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
            
            createListeners();
            
            dialog.setOnShowListener(new OnShowListener(){

				@Override
				public void onShow(DialogInterface arg0) {
					bAddUser = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
				}
            	
            });
            
            return dialog;
    	}
    	
    	private void createListeners() {
    		TextWatcher etListener;
			
			etListener = new TextWatcher() {
				
				@Override
				public void onTextChanged(
						CharSequence s, int start, int before, int count) 
				{
					
				}
				
				@Override
				public void beforeTextChanged(
						CharSequence s, int start, int count, int after) 
				{
					
				}
				
				@Override
				public void afterTextChanged(Editable s) {
					approveName();
				}
			};
			
			etAddUser.addTextChangedListener(etListener);
    	}
    	
    	private void approveName() {
    		String      userName;
    		Set<String> userNames;
    		
    		// TODO: don't load user names every single time?
    		userName  = etAddUser.getText().toString();
    		userNames = Database.getAllUserNames();
    		
    		// TODO: check if user already joined archive
    		if (userNames.contains(userName)) {
    			this.setMessage(
    					getResources().getString(
    							R.string.add_user_name_valid), 
    					true);
    		}
    		else {
    			this.setMessage(
    					getResources().getString(
    							R.string.add_user_name_invalid), 
    					false);
    		}
    	}
    	
    	private void setMessage(String message, boolean approved) {
    		tvAddUserMessage.setText(message);
    		if (approved) {
    			tvAddUserMessage.setTextColor(
    					getResources().getColor(R.color.green));
    			bAddUser.setEnabled(true);
    		}
    		else {
    			tvAddUserMessage.setTextColor(
    					getResources().getColor(R.color.red));
    			bAddUser.setEnabled(false);
    		}
    	}
    	
    	private void notifyUser(String invitedUserName) {
    		Activity                   activity;
    		Intent                     intent;
    		Intent                     resultIntent;
    		String                     userName;
    		String                     archiveName;
    		StringBuilder              notificationText;
    		NotificationCompat.Builder mBuilder;
    		TaskStackBuilder           stackBuilder;
    		PendingIntent              resultPendingIntent;
    		NotificationManager        mNotificationManager;
    		
    		activity         = getActivity();
    		intent           = activity.getIntent();
    		userName         = intent.getStringExtra(Keys.USERNAME);
    		archiveName      = intent.getStringExtra(Keys.ARCHIVENAME);
    		notificationText = new StringBuilder();
    		
    		notificationText
    			.append(archiveName)
    			.append(" (")
    			.append(userName)
    			.append(")");
    		
    		mBuilder = new NotificationCompat.Builder(getActivity())
    		        .setSmallIcon(R.drawable.ic_launcher)
    		        .setContentTitle(getResources().getString(
    		        		R.string.archive_invitation))
    		        .setContentText(notificationText.toString())
    		        .setAutoCancel(true);
    		// Creates an explicit intent for an Activity in your app
    		resultIntent = 
    				new Intent(activity, ArchiveOverview.class);
    		
    		resultIntent
    			.putExtra(Keys.USERNAME, invitedUserName)
    			.putExtra(Keys.INVITING_USERNAME, userName)
    			.putExtra(Keys.ARCHIVENAME, archiveName);
    		
    		// The stack builder object will contain an artificial back stack 
    		// for the started Activity.
    		// This ensures that navigating backward from the Activity leads 
    		// out of your application to the Home screen.
    		stackBuilder = TaskStackBuilder.create(activity);
    		// Adds the back stack for the Intent (but not the Intent itself)
    		stackBuilder.addParentStack(Login.class);
    		// Adds the Intent that starts the Activity to the top of the stack
    		stackBuilder.addNextIntent(resultIntent);
    		resultPendingIntent =
    		        stackBuilder.getPendingIntent(
    		            0,
    		            PendingIntent.FLAG_UPDATE_CURRENT
    		        );
    		mBuilder.setContentIntent(resultPendingIntent);
    		
    		mNotificationManager = (NotificationManager) 
    				activity.getSystemService(Context.NOTIFICATION_SERVICE);
    		
			mNotificationManager.notify(0, mBuilder.build());
    	}
    }
    
    public static class NewSongDialogFragment extends DialogFragment {
    	
    	private EditText etSongName;
    	private TextView tvSongNameMessage;
    	private Button   bCreate;
    	
    	@Override
    	public Dialog onCreateDialog(Bundle savedInstancesState) {
            AlertDialog.Builder builder;
            LayoutInflater      inflater;
            final View          dialogView;
            final AlertDialog   dialog;
            
            builder           = new AlertDialog.Builder(getActivity());
            inflater          = getActivity().getLayoutInflater();
            dialogView        = 
            		inflater.inflate(R.layout.d_new_song_dialog, null);
            etSongName        = (EditText) 
            		dialogView.findViewById(R.id.etSongName);
            tvSongNameMessage = (TextView) 
            		dialogView.findViewById(R.id.tvSongNameMessage);
            
            builder.setView(dialogView)
            	   .setTitle(R.string.new_song_dialog_title)
                   .setPositiveButton(R.string.create, 
                		   new DialogInterface.OnClickListener() {
		                       public void onClick(
		                    		   DialogInterface dialog, int id) 
		                       {
		                    	   Activity activity;
		                    	   Intent   currentIntent;
		                    	   Intent   intent;
		                    	   String   userName;
		                    	   String   archiveName;
		                    	   String   songName;
		                    	   
		                    	   activity      = getActivity();
		                    	   currentIntent = activity.getIntent();
		                    	   intent        = 
		                    			   new Intent(
		                    					   activity, 
		                    					   EditSongText.class);
		                    	   songName      = 
		                    			   etSongName.getText().toString();
		                    	   userName      = 
		                    			   currentIntent.getStringExtra(
		                    					   Keys.USERNAME);
		                    	   archiveName   = 
		                    			   currentIntent.getStringExtra(
		                    					   Keys.ARCHIVENAME);
		                    	   
		                    	   intent.putExtra(Keys.USERNAME, userName);
		                    	   intent.putExtra(
		                    			   Keys.ARCHIVENAME, archiveName);
		                    	   intent.putExtra(Keys.SONGNAME, songName);
		                    	   
		                    	   dialog.dismiss();
		                    	   startActivity(intent);
		                       }
                   		   })
                   .setNegativeButton(R.string.cancel, 
                		   new DialogInterface.OnClickListener() {
		                       public void onClick(
		                    		   DialogInterface dialog, int id) 
		                       {
		                           dialog.cancel();
		                       }
                   		   });
            
            dialog  = builder.create();
            bCreate = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
            
            createListeners();
            
            dialog.setOnShowListener(new OnShowListener(){

				@Override
				public void onShow(DialogInterface arg0) {
					bCreate = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
				}
            	
            });
            
            return dialog;
    	}
    	
    	private void createListeners() {
    		TextWatcher etListener;
			
			etListener = new TextWatcher() {
				
				@Override
				public void onTextChanged(
						CharSequence s, int start, int before, int count) 
				{
					
				}
				
				@Override
				public void beforeTextChanged(
						CharSequence s, int start, int count, int after) 
				{
					
				}
				
				@Override
				public void afterTextChanged(Editable s) {
					approveName();
				}
			};
			
			etSongName.addTextChangedListener(etListener);
    	}
    	
    	private void approveName() {
    		Intent       intent;
    		String       newSongName;
    		String       userName;
    		String       archiveName;
    		List<String> songNames;
    		
    		// TODO: don't load archive names every single time?
    		intent      = getActivity().getIntent();
    		newSongName = etSongName.getText().toString();
    		userName    = intent.getStringExtra(Keys.USERNAME);
    		archiveName = intent.getStringExtra(Keys.ARCHIVENAME);
    		songNames   = 
    				Database.getArchive(userName, archiveName)
    					.getAllSongNames();
    		
    		if (!MessageUtils.checkName(newSongName)) {
    			this.setMessage(
    					getResources().getString(R.string.song_name_invalid), 
    					false);
    		}
    		else if (songNames.contains(newSongName)) {
    			this.setMessage(
    					getResources().getString(R.string.song_name_taken), 
    					false);
    		}
    		else {
    			this.setMessage(
    					getResources().getString(R.string.song_name_valid), 
    					true);
    		}
    	}
    	
    	private void setMessage(String message, boolean approved) {
    		tvSongNameMessage.setText(message);
    		if (approved) {
    			tvSongNameMessage.setTextColor(
    					getResources().getColor(R.color.green));
    			bCreate.setEnabled(true);
    		}
    		else {
    			tvSongNameMessage.setTextColor(
    					getResources().getColor(R.color.red));
    			bCreate.setEnabled(false);
    		}
    	}
    }
    
    public static class RemoveArchiveDialogFragment extends DialogFragment {
    	
    	@Override
    	public Dialog onCreateDialog(Bundle savedInstancesState) {
            Intent              intent;
            final String        userName;
            final String        archiveName;
            AlertDialog.Builder builder;
            
            intent      = getActivity().getIntent();
			userName    = intent.getStringExtra(Keys.USERNAME);
			archiveName = intent.getStringExtra(Keys.ARCHIVENAME);
            builder     = new AlertDialog.Builder(getActivity());
            
            builder.setTitle(R.string.confirm_remove_archive)
                   .setPositiveButton(R.string.remove, 
                		   new DialogInterface.OnClickListener() {
		                       public void onClick(
		                    		   DialogInterface dialog, int id) 
		                       {
		                    	   User user;
		                    	   
		                    	   // remove archive
		                    	   user = Database.getUserData(userName);
		                    	   user.removeArchive(archiveName);
		                    	   
		                    	   // go back to archive overview
								   getActivity().finish();
		                    	   dialog.dismiss();
		                       }
                   		   })
                   .setNegativeButton(R.string.cancel, 
                		   new DialogInterface.OnClickListener() {
		                       public void onClick(
		                    		   DialogInterface dialog, int id) 
		                       {
		                           dialog.cancel();
		                       }
                   		   });
            
            return builder.create();
    	}
	}
}
