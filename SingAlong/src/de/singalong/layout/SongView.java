package de.singalong.layout;

import de.singalong.R;
import de.singalong.data.Database;
import de.singalong.data.Song;
import de.singalong.utils.Keys;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;


public class SongView extends MyActionBarActivity {

	private SongViewFragment frag;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.a_song_view);
		
        if (savedInstanceState == null) {
        	frag = new SongViewFragment();
        	
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.flSongView, frag)
                    .commit();
        }
        else {
        	frag = null;
        }
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.song_view, menu);
		return true;
	}
	
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        
        switch (id) {
        case R.id.action_recording:
        	frag.showRecording(true);
        	return true;
        case R.id.action_transpose:
        	frag.showTranspose(true);
        	return true;
        case R.id.action_chord_tables:
        	frag.showChordTables(true);
        	return true;
        case R.id.action_edit:  
        	Intent intent;
        	Intent newIntent;
        	
        	intent    = getIntent();
        	newIntent = new Intent(SongView.this, EditSongText.class);
        	
        	newIntent.putExtra(
        			Keys.USERNAME, intent.getStringExtra(Keys.USERNAME));
        	newIntent.putExtra(
        			Keys.ARCHIVENAME, intent.getStringExtra(Keys.ARCHIVENAME));
        	newIntent.putExtra(
        			Keys.SONGNAME, intent.getStringExtra(Keys.SONGNAME));
        	
            startActivity(newIntent);
            return true;
        default:
        	return super.onOptionsItemSelected(item);
        }
    }
	
	public void cancelRecordingButtonListener(View v) {
		frag.showRecording(false);
	}
	
	public void cancelTransposeButtonListener(View v) {
		frag.showTranspose(false);
	}
	
	public void cancelChordTablesButtonListener(View v) {
		frag.showChordTables(false);
	}

	public static class SongViewFragment extends Fragment {

		private TextView       songTextView;
		private RelativeLayout recordingLayout;
    	private RelativeLayout transposeLayout;
    	private RelativeLayout chordTablesLayout;
    	private Song           song;
		
        public SongViewFragment() {
        }

        @Override
        public View onCreateView(
        		LayoutInflater inflater, 
        		ViewGroup container,
                Bundle savedInstanceState) 
        {
        	View rootView;
        	
        	rootView = inflater.inflate(R.layout.f_song_view, container, false);
        	
        	createSongView(rootView);
            return rootView;
        }
        
        private void createSongView(View view) {
        	Intent intent;
        	String userName;
        	String archiveName;
        	String songName;
        	
        	intent      = getActivity().getIntent();
			userName    = intent.getStringExtra(Keys.USERNAME);
			archiveName = intent.getStringExtra(Keys.ARCHIVENAME);
			songName    = intent.getStringExtra(Keys.SONGNAME);
			song        = Database.getSong(userName, archiveName, songName);
			
			getActivity().getActionBar().setTitle(song.getName());
			
			// get view elements
			songTextView      = 
					(TextView) view.findViewById(R.id.tvSongText);
			recordingLayout   = 
					(RelativeLayout) view.findViewById(R.id.rlRecording);
			transposeLayout   = 
					(RelativeLayout) view.findViewById(R.id.rlTranspose);
			chordTablesLayout = 
					(RelativeLayout) view.findViewById(R.id.rlChordTables);
			
			// make all additional views invisible
			showRecording(false);
			showTranspose(false);
			showChordTables(false);
			
			// add listeners
			createListeners(view);
			
			// make text scrollable
			songTextView.setMovementMethod(new ScrollingMovementMethod());
			
			// set song text
			refreshTextView();
			
        }
        
        private void createListeners(View view) {
        	ImageButton bTransposeUp;
        	ImageButton bTransposeDown;
        	
        	bTransposeUp   = 
        			(ImageButton) view.findViewById(R.id.ibTransposeUp);
        	bTransposeDown = 
        			(ImageButton) view.findViewById(R.id.ibTransposeDown);
        	
        	bTransposeUp.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {					
					song.transposeUp();
					SongViewFragment.this.refreshTextView();
				}
			});
        	
        	bTransposeDown.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					song.transposeDown();
					SongViewFragment.this.refreshTextView();
				}
			});
        }
        
        public void refreshTextView() {
        	songTextView.setText(song.getText(false));
        }
        
        public void showRecording(boolean status) {
        	if (status) {
        		recordingLayout.setVisibility(View.VISIBLE);
        	}
        	else {
        		recordingLayout.setVisibility(View.GONE);
        	}
        }
        
        public void showTranspose(boolean status) {
        	if (status) {
        		transposeLayout.setVisibility(View.VISIBLE);
        	}
        	else {
        		transposeLayout.setVisibility(View.GONE);
        	}
        }
        
        public void showChordTables(boolean status) {
        	if (status) {
        		chordTablesLayout.setVisibility(View.VISIBLE);
        	}
        	else {
        		chordTablesLayout.setVisibility(View.GONE);
        	}
        }
	}
}