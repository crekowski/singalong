package de.singalong.layout;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import de.singalong.R;
import de.singalong.data.Archive;
import de.singalong.data.Database;
import de.singalong.data.Line;
import de.singalong.data.Section;
import de.singalong.data.Song;
import de.singalong.utils.Keys;
import de.singalong.utils.SectionType;
import de.singalong.utils.SectionTypeUtils;
import de.singalong.utils.SpanUtils;

public class EditStructure extends MyActionBarActivity {

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_edit_structure);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.flEditStructure, new EditStructureFragment())
                    .commit();
        }
    }
	
	public static class EditStructureFragment extends Fragment {

        public EditStructureFragment() {
        }

        @Override
        public View onCreateView(
        		LayoutInflater inflater, 
        		ViewGroup container,
                Bundle savedInstanceState) 
        {
            View   rootView;
            Intent intent;
            String songName;
            
            rootView = inflater.inflate(
            		R.layout.f_edit_structure, container, false);            
            intent   = getActivity().getIntent();
			songName = intent.getStringExtra(Keys.SONGNAME);
			
			getActivity().getActionBar().setTitle(songName);
			
			EditText etStructure = (EditText) rootView.findViewById(R.id.etStructure);
			etStructure.setText("\n" + intent.getStringExtra(Keys.SONGTEXT));
			
            createListeners(rootView);
            createStructureTags(rootView);
            
            return rootView;
        }
        
        private void createListeners(View view) {
        	
        	EditText etStructure = (EditText) view.findViewById(R.id.etStructure);
        	final InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getBaseContext().getSystemService(Activity.INPUT_METHOD_SERVICE);

        	etStructure.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					deleteSection();
					EditText etStructure = ((EditText) v);
					Layout layout = etStructure.getLayout();
					int curPos = etStructure.getSelectionStart();
					int curLine = layout.getLineForOffset(curPos);
					int startPos = layout.getLineStart(curLine);
					if (curPos == startPos) {
						etStructure.setSelection(Math.max(0, curPos));						
					}
					else {
						etStructure.setSelection(Math.max(0, startPos - 1));
					}
		            inputMethodManager.hideSoftInputFromWindow(etStructure.getWindowToken(), 0);
				}
			});
        	
        	ImageView goBackToText = 
        			(ImageView) view.findViewById(R.id.ivGoBackToText);
        	
        	goBackToText.setOnClickListener(new View.OnClickListener() {
        		
        		@Override
        		public void onClick(View v) {
        			// save changes        			
        			Intent intent = new Intent(getActivity(), EditSongText.class);
        			
		        	intent.putExtra(
		        			Keys.SONGNAME, 
		        			getActivity().getIntent().getStringExtra(
		        					Keys.SONGNAME));
		        	
		            startActivity(intent);
        			getActivity().finish();				
        		}
        	});
        	
        	ImageView finishedStructure = (ImageView) view.findViewById(R.id.ivFinishedStructure);
        	
        	finishedStructure.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					saveSong(getActivity().getIntent(), true);
					getActivity().finish();
				}
			});
        	
        	ImageView goToChords = (ImageView) view.findViewById(R.id.ivGoToChords);
        	        	
        	goToChords.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent intent = getActivity().getIntent();
					saveSong(intent, false);										
					Intent newIntent = new Intent(getActivity(), EditChords.class);
					newIntent.putExtra(Keys.USERNAME, 
		        			intent.getStringExtra(Keys.USERNAME));
		        	newIntent.putExtra(Keys.ARCHIVENAME, 
		        			intent.getStringExtra(Keys.ARCHIVENAME));
		        	newIntent.putExtra(Keys.SONGNAME, 
		        			intent.getStringExtra(Keys.SONGNAME));
		            startActivity(newIntent);	
					getActivity().finish();
				}
			});
        }
        
        private void createStructureTags(View view) {
        	
        	Button bIntro = (Button) view.findViewById(R.id.bIntro);
        	
        	bIntro.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					createSection(getString(R.string.structure_intro));
				}
			});
        	
        	Button bVerse = (Button) view.findViewById(R.id.bVerse);
        	
        	bVerse.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					createSection(getString(R.string.structure_verse));
				}
			});
        	
        	Button bPrechorus = (Button) view.findViewById(R.id.bPrechorus);
        	
        	bPrechorus.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					createSection(getString(R.string.structure_prechorus));
				}
			});
        	
        	Button bChorus = (Button) view.findViewById(R.id.bChorus);
        	
        	bChorus.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					createSection(getString(R.string.structure_chorus));
				}
			});
        	
        	Button bBridge = (Button) view.findViewById(R.id.bBridge);
        	
        	bBridge.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					createSection(getString(R.string.structure_bridge));
				}
			});
        	
        	Button bInterlude = (Button) view.findViewById(R.id.bInterlude);
        	
        	bInterlude.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					createSection(getString(R.string.structure_interlude));
				}
			});
        	
        	Button bOutro = (Button) view.findViewById(R.id.bOutro);
        	
        	bOutro.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					createSection(getString(R.string.structure_outro));
				}
			});
        	
        	Button bOther = (Button) view.findViewById(R.id.bOther);
        	
        	bOther.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					createSection(getString(R.string.structure_other));
				}
			});
        }
        
        private void createSection(String section) {
        	EditText  etStructure;
        	Spannable tag;
        	Drawable  x;
        	
        	etStructure = 
        			(EditText) getActivity().findViewById(R.id.etStructure);
            
			if (etStructure.getSelectionStart() == 0) {
				tag = SpanUtils.setSize("[" + section + "]^^^", 1.2f);
			} else {
				tag = SpanUtils.setSize("\n[" + section + "]^^^", 1.2f);				
			}
			tag = SpanUtils.setColor(tag, Color.GRAY);
			
			x = getResources().getDrawable(R.drawable.ic_action_cancel_dark);
			x.setBounds(0, 0, x.getIntrinsicWidth(), x.getIntrinsicHeight());
			
			tag.setSpan(
					new ImageSpan(x), 
					tag.length() - 3, 
					tag.length(), 
					Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
			
			etStructure.getText().insert(etStructure.getSelectionStart(), tag);
        }
        
        private void deleteSection() {
        	EditText etStructure = (EditText) getActivity().findViewById(R.id.etStructure);
        	int curPos = etStructure.getSelectionStart();
        	SpannableStringBuilder text = new SpannableStringBuilder(etStructure.getText());
        	if ((curPos < text.length() && text.charAt(curPos) == '^') || 
        			(curPos > 0 && text.charAt(curPos-1) == '^')) { 
        		Layout layout = etStructure.getLayout();
        		int curLine = layout.getLineForOffset(curPos);
        		int startTag = layout.getLineStart(curLine);
        		int endTag = layout.getLineEnd(curLine);
        		text.removeSpan(text.getSpans(startTag, endTag, ImageSpan.class));
        		text.replace(Math.max(0, startTag - 1), endTag - 1, "");
        		etStructure.setText(text);
        	}
        }
        
        private void saveSong(Intent intent, boolean finished) {
			Song song = new Song(getActivity().getIntent().
					getStringExtra(Keys.SONGNAME));
			EditText etStructure = (EditText) getActivity().findViewById(R.id.etStructure);
			String text = etStructure.getText().toString();
			String[] lines = text.split("\n");
			Section curSec = null;
			for (String line : lines) {
				if (line.equals("")) {
					//ignore
				}
				else if (line.charAt(0) == '[') {
					if (curSec != null) {
						song.addSection(curSec);								
					}
					curSec = new Section(SectionTypeUtils.sectionTypeFromString(line.replace("^^^", "")));
				}
				else {
					String chordLine = "";
					if (finished) {
						chordLine = " ";
					}
					else {						
						for (int i = 0; i < line.length(); i++) {
							chordLine += " ";
						}
					}
					if (curSec == null) {
						curSec = new Section(SectionType.OTHER);
					}
					curSec.addLine(new Line(chordLine, line));
				}
			}
			
			song.addSection(curSec);
			Archive archive = Database.getArchive(intent.getStringExtra(Keys.USERNAME), intent.getStringExtra(Keys.ARCHIVENAME));
			archive.addSong(song);
        }
	}
}
