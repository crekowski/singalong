package de.singalong.layout;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import de.singalong.R;
import de.singalong.data.Archive;
import de.singalong.data.Database;
import de.singalong.data.Line;
import de.singalong.data.Section;
import de.singalong.data.Song;
import de.singalong.utils.Keys;
import de.singalong.utils.SectionType;

public class EditSongText extends MyActionBarActivity {

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_edit_text);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.flEditText, new EditTextFragment())
                    .commit();
        }
    }
	
	public static class EditTextFragment extends Fragment {

        public EditTextFragment() {
        }

        @Override
        public View onCreateView(
        		LayoutInflater inflater, 
        		ViewGroup container,
                Bundle savedInstanceState) 
        {
            View   rootView;
            Intent intent;
            String songName;
            
            rootView = inflater.inflate(R.layout.f_edit_text, container, false); 
            intent   = getActivity().getIntent();
			songName = intent.getStringExtra(Keys.SONGNAME);
			
			getActivity().getActionBar().setTitle(songName);
            
			createListeners(rootView);  
            setExampleSong(rootView);
            
            return rootView;
        }
        
        private void createListeners(View view) {
        	
        	ImageView finishedText = (ImageView) view.findViewById(R.id.ivFinishedText);
        	
        	finishedText.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					saveSong(getActivity().getIntent());
					getActivity().finish();
				}
			});
        	
        	ImageView goToStructure = 
        			(ImageView) view.findViewById(R.id.ivGoToStructure);
        	        	
        	goToStructure.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					EditText etSongtext = (EditText) getActivity().findViewById(R.id.etText);				
					Intent intent = getActivity().getIntent();
		        	Intent newIntent = new Intent(getActivity(), EditStructure.class);
		        	newIntent.putExtra(Keys.USERNAME, 
		        			intent.getStringExtra(Keys.USERNAME));
		        	newIntent.putExtra(Keys.ARCHIVENAME, 
		        			intent.getStringExtra(Keys.ARCHIVENAME));
		        	newIntent.putExtra(Keys.SONGNAME, 
		        			intent.getStringExtra(Keys.SONGNAME));
		        	newIntent.putExtra(Keys.SONGTEXT, 
		        			etSongtext.getText().toString());
		            startActivity(newIntent);
					getActivity().finish();
				}
			});
        	
        }
        
        private void saveSong(Intent intent) {
			Song song = new Song(getActivity().getIntent().
					getStringExtra(Keys.SONGNAME));
			EditText etText = 
					(EditText) getActivity().findViewById(R.id.etText);
			
			String text    = etText.getText().toString();
			String[] lines = text.split("\n");
			
			Section curSec = new Section(SectionType.OTHER);
			for (String line : lines) {
				if (line.equals("") == false) {
					curSec.addLine(new Line("", line));
				}
			}
			
			song.addSection(curSec);
			Archive archive = Database.getArchive(intent.getStringExtra(Keys.USERNAME), intent.getStringExtra(Keys.ARCHIVENAME));
			archive.addSong(song);
        }
        
        private void setExampleSong(View view) {
        	android.widget.EditText text = (android.widget.EditText) view.findViewById(R.id.etText);
        	
        	text.setText(
					"Whoa oh whoa oh\n" +
					"Whoa oh whoa oh\n" +
					"Vengaboys are back in town\n" +
					"Whoa oh whoa oh\n" +
					"Whoa oh whoa oh\n" +
					"Whoa oh whoa oh\n" +
					"Whoa oh whoa oh\n" +
					"\n" +
					"If you're alone and you \n" +
					"need a friend\n" +
					"Someone to make you forget \n" +
					"your problems\n" +
					"Just come along baby\n" +
					"Take my hand\n" +
					"I'll be your lover tonight\n" +
					"\n" +
					"Whoa oh whoa oh\n" +
					"This is what I wanna do\n" +
					"Whoa oh whoa oh\n" +
					"Let's have some fun\n" +
					"Whoa oh whoa oh\n" +
					"One on one just me and you\n" +
					"Whoa oh whoa oh\n" +
					"\n" +
					"Boom boom boom boom\n" +
					"I want you in my room\n" +
					"Let's spend the night \n" +
					"together\n" +
					"From now until forever\n" +
					"Boom boom boom boom\n" +
					"I wanna double boom\n" +
					"Let's spend the night together\n" +
					"Together in my room\n" +
					"\n" +
					"Whoa oh whoa oh\n" +
					"Everybody get on down\n" +
					"Whoa oh whoa oh\n" +
					"Vengaboys are back in town\n" +
					"\n" +
					"Woooo!\n" +
					"Woooo woooo!\n" +
					"Woooo!\n" +
					"Woooo woooo!\n" +
					"Woooo!\n" +
					"Woooo woooo!\n" +
					"Woooo!\n" +
					"Woooo woooo!\n" +
					"\n" +
					"Whoa oh whoa oh\n" +
					"Woooo woooo!\n" +
					"Whoa oh whoa oh\n" +
					"Woooo woooo!\n" +
					"Whoa oh whoa oh\n" +
					"Woooo woooo!\n" +
					"Whoa oh whoa oh\n"
					);
        }
	}
}
