package de.singalong.layout;

import de.singalong.R;
import de.singalong.utils.Keys;
import de.singalong.utils.MessageUtils;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Login extends MyActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.a_login);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.flLogin, new LoginFragment()).commit();
		}
	}
	
	
	public static class LoginFragment extends Fragment {
		
		private EditText etUserName;
		private TextView tvUserNameMessage;
		private Button   bLogin;
		
		public LoginFragment() {
		}

		@Override
		public View onCreateView(
				LayoutInflater inflater, 
				ViewGroup container,
				Bundle savedInstanceState) 
		{
			View rootView;
			
			rootView = inflater.inflate(R.layout.f_login, container, false);
			
			
			// get view elements
			etUserName        = 
					(EditText) rootView.findViewById(R.id.etUserName);
			tvUserNameMessage = 
					(TextView) rootView.findViewById(R.id.tvUserNameMessage);
			bLogin            = 
					(Button)   rootView.findViewById(R.id.bLogin);
			
			setMessage("", false);
			
			createListeners(rootView);
			
			return rootView;
		}
		
		private void createListeners(View view) {
			TextWatcher          etListener;
			View.OnClickListener bListener;
			final View           finalView;
			
			
			// set listener for the edit text
			etListener = new TextWatcher() {
				
				@Override
				public void onTextChanged(
						CharSequence s, int start, int before, int count) 
				{
					
				}
				
				@Override
				public void beforeTextChanged(
						CharSequence s, int start, int count, int after) 
				{
					
				}
				
				@Override
				public void afterTextChanged(Editable s) {
					approveName();
				}
			};
			
			etUserName.addTextChangedListener(etListener);
			
			
			// set listener for the login button
			finalView = view;
			bListener = new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent   intent;
					EditText etUserName;
					String   userName;
					
					intent     = 
							new Intent(getActivity(), ArchiveOverview.class);
					etUserName = 
							(EditText) finalView.findViewById(R.id.etUserName);
					userName   = etUserName.getText().toString();
					
					intent.putExtra(Keys.USERNAME, userName);
					
				    startActivity(intent);
				}
			};
			
			bLogin.setOnClickListener(bListener);
		}
		
		private void approveName() {
			String userName;
			
			userName = etUserName.getText().toString();
			
			if (!MessageUtils.checkName(userName)) {
				this.setMessage(
						getResources().getString(R.string.user_name_invalid), 
						false);
			}
			else if (userName.equals("TimSabsch")) {
				this.setMessage(
						getResources().getString(R.string.user_name_taken), 
						false);
			}
			else {
				this.setMessage(
						getResources().getString(R.string.user_name_valid), 
						true);
			}
		}
		
		private void setMessage(String message, boolean approved) {
			tvUserNameMessage.setText(message);
			if (approved) {
				tvUserNameMessage.setTextColor(
						getResources().getColor(R.color.green));
				bLogin.setEnabled(true);
			}
			else {
				tvUserNameMessage.setTextColor(
						getResources().getColor(R.color.red));
				bLogin.setEnabled(false);
			}
		}
	}

}
