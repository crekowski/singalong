package de.singalong.layout;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import de.singalong.R;
import de.singalong.data.Archive;
import de.singalong.data.Database;
import de.singalong.data.Line;
import de.singalong.data.Section;
import de.singalong.data.Song;
import de.singalong.utils.Keys;
import de.singalong.utils.SectionTypeUtils;

public class EditStrumming extends MyActionBarActivity {

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_edit_strumming);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.flEditStrumming, new EditStrummingFragment())
                    .commit();
        }
    }
	
	public static class EditStrummingFragment extends Fragment {

        public EditStrummingFragment() {
        }

        @Override
        public View onCreateView(
        		LayoutInflater inflater, 
        		ViewGroup container,
                Bundle savedInstanceState) 
        {
            View   rootView;
            Intent intent;
            String songName;
            
            rootView = inflater.inflate(
            		R.layout.f_edit_strumming, container, false);
            intent   = getActivity().getIntent();
			songName = intent.getStringExtra(Keys.SONGNAME);
			
			getActivity().getActionBar().setTitle(songName);
			
            initTabWidget(rootView);
            createListeners(rootView);
            
            return rootView;
        }
        
        private void initTabWidget(View view) {
        	TabHost host;
        	TabSpec tabW;
        	TabSpec tabX;
        	TabSpec tabY;
        	TabSpec tabZ;
        	
        	host = (TabHost) view.findViewById(R.id.thStrumming);
        	host.setup();

        	tabW = host.newTabSpec("Up");
        	tabW.setContent(R.id.llStrummingUp);
        	tabW.setIndicator("^");
        	host.addTab(tabW);
        	
        	tabX = host.newTabSpec("Down");
        	tabX.setContent(R.id.llStrummingDown);
        	tabX.setIndicator("v");
        	host.addTab(tabX);
        	
        	tabY = host.newTabSpec("Tap");
        	tabY.setContent(R.id.llStrummingTap);
        	tabY.setIndicator("|");
        	host.addTab(tabY);
        	
        	tabZ = host.newTabSpec("Break");
        	tabZ.setContent(R.id.llStrummingBreak);
        	tabZ.setIndicator("*");
        	host.addTab(tabZ);
        }
        
        private void createListeners(View view) {
        	
        	EditText etStrumming = (EditText) view.findViewById(R.id.etStrumming);
			
			etStrumming.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO implement
				}	
			});
        	
        	ImageView goBackToChords = 
        			(ImageView) view.findViewById(R.id.ivGoBackToChords);
        	
        	goBackToChords.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent intent = 
							new Intent(getActivity(), EditChords.class);
					
		        	intent.putExtra(
		        			Keys.SONGNAME, 
		        			getActivity().getIntent().getStringExtra(
		        					Keys.SONGNAME));
		        	
		            startActivity(intent);
					getActivity().finish();
				}
			});
        	
        	ImageView finishedStrumming = 
        			(ImageView) view.findViewById(R.id.ivFinishedStrumming);
        	
        	finishedStrumming.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					saveSong(getActivity().getIntent());
					getActivity().finish();				
				}
			});
        }
        
        private void saveSong(Intent intent) {
			Song song = new Song(getActivity().getIntent().
					getStringExtra(Keys.SONGNAME) + "2");
			EditText etStructure = (EditText) getActivity().findViewById(R.id.etStructure);
			String text = etStructure.getText().toString();
			String[] lines = text.split("\n");
			Section curSec = null;
			for (String line : lines) {
				if (line.equals("")) {
					//ignore
				}
				else if (line.charAt(0) == '[') {
					if (curSec != null) {
						song.addSection(curSec);								
					}
					curSec = new Section(SectionTypeUtils.sectionTypeFromString(line));
				}
				else {
					String chordLine = "";
					for (int i = 0; i < line.length(); i++) {
						chordLine += " ";
					}
					chordLine += "\n";
					curSec.addLine(new Line(chordLine, line));
				}
			}
			
			Archive archive = Database.getArchive(intent.getStringExtra(Keys.USERNAME), intent.getStringExtra(Keys.ARCHIVENAME));
			archive.addSong(song);
        }
	}
}
