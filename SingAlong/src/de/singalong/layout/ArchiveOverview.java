package de.singalong.layout;

import java.util.List;

import de.singalong.R;
import de.singalong.data.Archive;
import de.singalong.data.Database;
import de.singalong.data.User;
import de.singalong.utils.Keys;
import de.singalong.utils.MessageUtils;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

public class ArchiveOverview extends MyActionBarActivity {
	
	ArchiveOverviewFragment frag;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_archive_overview);
        
        frag = new ArchiveOverviewFragment();

        if (savedInstanceState == null) {
            getSupportFragmentManager()
            		.beginTransaction()
                    .add(R.id.flArchiveOverview, frag)
                    .commit();
        }
        
        if (getIntent().getStringExtra(Keys.INVITING_USERNAME) != null) {
        	InvitationDialogFragment dialog;
    		
    		dialog = new InvitationDialogFragment();
    		dialog.setArchiveOverview(frag);
    		dialog.show(getSupportFragmentManager(), "InvitationDialogFragment");
        }
    }
    
    public void newArchiveButtonListener(View v) {
		NewArchiveDialogFragment dialog;
		
		dialog = new NewArchiveDialogFragment();
		dialog.setArchiveOverview(frag);
		dialog.show(getSupportFragmentManager(), "NewArchiveDialogFragment");
	}
    
    
    public static class ArchiveOverviewFragment 
    		extends Fragment 
    {

    	private ArrayAdapter<String> archiveListAdapter;
    	private User                 user;
    	
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) 
        {
            View rootView;
            
            rootView = inflater.inflate(
            		R.layout.f_archive_overview, container, false);
            
            createOverview(rootView);
            return rootView;
        }
        
        private void createOverview(View view) {
        	final String       userName;
        	ListView           archiveList;
        	final List<String> archiveOverview;
        	
        	userName           = 
        			getActivity().getIntent().getStringExtra(Keys.USERNAME);
        	user               = Database.getUserData(userName);
        	archiveList        = (ListView) view.findViewById(R.id.lvArchives);
        	archiveOverview    = user.getAllArchiveNames();
    		archiveListAdapter = 
    				new ArrayAdapter<String>(
    						getActivity(),
    						android.R.layout.simple_list_item_1, 
    						archiveOverview);

    		// Assign adapter to ListView
    		archiveList.setAdapter(archiveListAdapter);

    		// ListView Item Click Listener
    		archiveList.setOnItemClickListener(new OnItemClickListener() {

    			@Override
    			public void onItemClick(
    					AdapterView<?> parent, View view, int position, long id) 
    			{

    				Intent intent = new Intent(getActivity(),
    							SongOverview.class);
    				
    				intent.putExtra(Keys.USERNAME, userName);
    				intent.putExtra(
    						Keys.ARCHIVENAME, archiveOverview.get(position));
    				
    				startActivity(intent);
    			}

    		});
    	}

        @Override
        public void onResume() {
        	super.onResume();
        	
        	refresh();
        }
        
		public void refresh() {
			// update archive list
			archiveListAdapter.clear();
			archiveListAdapter.addAll(user.getAllArchiveNames());
		}
		
		public List<String> getArchiveNames() {
			return user.getAllArchiveNames();
		}
    }
    
    public static class NewArchiveDialogFragment extends DialogFragment {
    	
    	private ArchiveOverviewFragment archiveList;
    	private EditText                etArchiveName;
    	private TextView                tvArchiveNameMessage;
    	private Button                  bCreate;
    	
    	public void setArchiveOverview(ArchiveOverviewFragment _frag) {
    		archiveList = _frag;
    	}
    	
    	@Override
    	public Dialog onCreateDialog(Bundle savedInstancesState) {
            AlertDialog.Builder builder;
            LayoutInflater      inflater;
            final View          dialogView;
            final AlertDialog   dialog;
            
            builder              = new AlertDialog.Builder(getActivity());
            inflater             = getActivity().getLayoutInflater();
            dialogView           = 
            		inflater.inflate(R.layout.d_new_archive_dialog, null);
            etArchiveName        = (EditText) 
            		dialogView.findViewById(R.id.etArchiveName);
            tvArchiveNameMessage = (TextView) 
            		dialogView.findViewById(R.id.tvArchiveNameMessage);
            
            builder.setView(dialogView)
            	   .setTitle(R.string.new_archive_dialog_title)
                   .setPositiveButton(R.string.create, 
                		   new DialogInterface.OnClickListener() {
		                       public void onClick(
		                    		   DialogInterface dialog, int id) 
		                       {
		                    	   String archiveName;
		                    	   String userName;
		                    	   User   user;
		                    	   
		                    	   archiveName = 
		                    			   etArchiveName.getText().toString();
		                    	   userName    = 
		                    			   getActivity()
		                    			   .getIntent()
		                    			   .getStringExtra(Keys.USERNAME);
		                    	   user        = Database.getUserData(userName);
		                    	   
		                    	   // add archive
		                    	   user.newArchive(archiveName);
		                    	   archiveList.refresh();
		                    	   
		                    	   dialog.dismiss();
		                       }
                   		   })
                   .setNegativeButton(R.string.cancel, 
                		   new DialogInterface.OnClickListener() {
		                       public void onClick(
		                    		   DialogInterface dialog, int id) 
		                       {
		                           dialog.cancel();
		                       }
                   		   });
            
            dialog  = builder.create();
            bCreate = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
            
            createListeners();
            
            dialog.setOnShowListener(new OnShowListener(){

				@Override
				public void onShow(DialogInterface arg0) {
					bCreate = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
				}
            	
            });
            
            return dialog;
    	}
    	
    	private void createListeners() {
    		TextWatcher etListener;
			
			etListener = new TextWatcher() {
				
				@Override
				public void onTextChanged(
						CharSequence s, int start, int before, int count) 
				{
					
				}
				
				@Override
				public void beforeTextChanged(
						CharSequence s, int start, int count, int after) 
				{
					
				}
				
				@Override
				public void afterTextChanged(Editable s) {
					approveName();
				}
			};
			
			etArchiveName.addTextChangedListener(etListener);
    	}
    	
    	private void approveName() {
    		String       newArchiveName;
    		List<String> archiveNames;
    		
    		// TODO: don't load archive names every single time?
    		newArchiveName = etArchiveName.getText().toString();
    		archiveNames   = archiveList.getArchiveNames();
    		
    		if (!MessageUtils.checkName(newArchiveName)) {
    			this.setMessage(
    					getResources().getString(R.string.archive_name_invalid), 
    					false);
    		}
    		else if (archiveNames.contains(newArchiveName)) {
    			this.setMessage(
    					getResources().getString(R.string.archive_name_taken), 
    					false);
    		}
    		else {
    			this.setMessage(
    					getResources().getString(R.string.archive_name_valid), 
    					true);
    		}
    	}
    	
    	private void setMessage(String message, boolean approved) {
    		tvArchiveNameMessage.setText(message);
    		if (approved) {
    			tvArchiveNameMessage.setTextColor(
    					getResources().getColor(R.color.green));
    			bCreate.setEnabled(true);
    		}
    		else {
    			tvArchiveNameMessage.setTextColor(
    					getResources().getColor(R.color.red));
    			bCreate.setEnabled(false);
    		}
    	}
    }
    
    public static class InvitationDialogFragment extends DialogFragment {
		
    	private ArchiveOverviewFragment archiveList;
    	
    	public void setArchiveOverview(ArchiveOverviewFragment _frag) {
    		archiveList = _frag;
    	}
    	
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			AlertDialog.Builder builder;
            Intent              intent;
            StringBuilder       title;
            final String        userName;
            final String        invitingUserName;
            final String        archiveName;
            
            builder          = new AlertDialog.Builder(getActivity());
            intent           = getActivity().getIntent();
            userName         = intent.getStringExtra(Keys.USERNAME);
            invitingUserName = intent.getStringExtra(Keys.INVITING_USERNAME);
            archiveName      = intent.getStringExtra(Keys.ARCHIVENAME);
            title            = new StringBuilder();
            
            title
            	.append(archiveName)
            	.append(" (")
            	.append(invitingUserName)
            	.append(") beitreten?");
            
            builder.setTitle(title.toString())
                   .setPositiveButton(R.string.accept, 
                		   new DialogInterface.OnClickListener() {
		                       public void onClick(
		                    		   DialogInterface dialog, int id) 
		                       {
		                    	   User    user;
		                    	   Archive archive;
		                    	   
		                    	   user    = Database.getUserData(userName);
		                    	   archive = Database.getArchive(
		                    			   invitingUserName, archiveName);
		                    	   
		                    	   user.addArchive(archive);
		                    	   archiveList.refresh();
		                    	   
		                    	   dialog.dismiss();
		                       }
                   		   })
                   .setNegativeButton(R.string.decline, 
                		   new DialogInterface.OnClickListener() {
		                       public void onClick(
		                    		   DialogInterface dialog, int id) 
		                       {
		                           dialog.cancel();
		                       }
                   		   });
            
            return builder.create();
		}
	}
}
