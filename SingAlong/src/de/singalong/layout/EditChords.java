package de.singalong.layout;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import de.singalong.R;
import de.singalong.data.Archive;
import de.singalong.data.Database;
import de.singalong.data.Line;
import de.singalong.data.Section;
import de.singalong.data.Song;
import de.singalong.utils.Keys;
import de.singalong.utils.SectionTypeUtils;
import de.singalong.utils.SpanUtils;

public class EditChords extends MyActionBarActivity {

	final static String[] tones = { "C", "D", "E", "F", "G", "A", "B" };
	final static String[] accidentals = { "", "#", "b" };
	final static String[] chords = { "", "m", "7", "m7", "maj7", "add9",
			"sus2", "sus4", "7sus4" };
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.a_edit_chords);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.flEditChords, new EditChordsFragment()).commit();
		}
	}

	public static class EditChordsFragment extends Fragment {

		public EditChordsFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView;

			rootView = inflater.inflate(R.layout.f_edit_chords, container,
					false);

			Intent intent = getActivity().getIntent();
			String songName = intent.getStringExtra(Keys.SONGNAME);

			getActivity().getActionBar().setTitle(songName);

			initTabWidget(rootView);
			createListeners(rootView);
			setExampleSong(rootView);
			
			return rootView;
		}
		
		@Override
		public void onStart() {
			super.onStart();
			final EditText etChords = (EditText) getActivity().findViewById(R.id.etChords);
			etChords.post(new Runnable() {

		        @Override
		        public void run() {
		        	Layout layout = etChords.getLayout();
		            highlightWord(etChords, layout.getLineStart(2), layout.getLineStart(1));
		        	etChords.requestFocus();
		        }
		    });
		}
		
		private void initTabWidget(View view) {
			TabHost host;
			host = (TabHost) view.findViewById(R.id.thChords);
			host.setup();

			TabSpec tab;
			for (final String tone : tones) {
				tab = host.newTabSpec(tone);
				tab.setIndicator(tone);
				tab.setContent(new TabHost.TabContentFactory() {
					public View createTabContent(String tag) {
						HorizontalScrollView hsv = new HorizontalScrollView(
								getActivity());
						LinearLayout hsvChild = new LinearLayout(getActivity());

						for (String chord : chords) {
							LinearLayout layout = new LinearLayout(
									getActivity());
							layout.setOrientation(LinearLayout.VERTICAL);
							for (String accidental : accidentals) {
								Button bChord = new Button(getActivity());
								bChord.setText(tone + accidental + chord);
								bChord.setOnClickListener(new View.OnClickListener() {								
									 @Override
									 public void onClick(View v) {
									 createChord(((Button) v).getText().toString());
									 }
								});
								layout.addView(bChord);
							}
							hsvChild.addView(layout);	
						}
						hsv.addView(hsvChild);
						return hsv;
					}
				});
				host.addTab(tab);
			}
		}

		private void createListeners(View view) {

			EditText etChords = (EditText) view.findViewById(R.id.etChords);
			final InputMethodManager inputMethodManager = (InputMethodManager) getActivity()
					.getBaseContext().getSystemService(
							Activity.INPUT_METHOD_SERVICE);

			etChords.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					EditText etChords = ((EditText) v);
					inputMethodManager.hideSoftInputFromWindow(
							etChords.getWindowToken(), 0);
					Layout layout = etChords.getLayout();

					int curPos = etChords.getSelectionStart();
					int curLine = layout.getLineForOffset(curPos);
					int startPos = layout.getLineStart(curLine);

					// empty line
					if (etChords.getText().charAt(startPos) == '\n') {
						startPos = layout.getLineStart(curLine + 3);
						int cursorPos = layout.getLineStart(curLine + 2);
						highlightWord(etChords, startPos, cursorPos);
					}
					// section type line
					else if (etChords.getText().charAt(startPos) == '[') {
						startPos = layout.getLineStart(curLine + 2);
						int cursorPos = layout.getLineStart(curLine + 1);
						highlightWord(etChords, startPos, cursorPos);
					}
					// text line
					else if (curLine % 2 == 0) {
						String text = etChords.getText().toString();
						int wordStartPos = Math.max(
								text.lastIndexOf(' ', curPos),
								text.lastIndexOf('\n', curPos)) + 1;
						int cursorPos = layout.getLineStart(curLine - 1)
								+ (wordStartPos - startPos);
						highlightWord(etChords, wordStartPos, cursorPos);
					}
					// chord line
					else {
						int nextLinePos = layout.getLineStart(curLine + 1)
								+ (curPos - startPos);
						String text = etChords.getText().toString();
						int wordStartPos = Math.max(
								text.lastIndexOf(' ', nextLinePos),
								text.lastIndexOf('\n', nextLinePos)) + 1;
						int cursorPos = startPos
								+ (wordStartPos - layout
										.getLineStart(curLine + 1));
						highlightWord(etChords, wordStartPos, cursorPos);
					}

					// TODO Akkorde ohne Text???
				}
			});

			
			ImageView goBackToStructure = (ImageView) view
					.findViewById(R.id.ivGoBackToStructure);

			goBackToStructure.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// save changes
					Intent intent = new Intent(getActivity(),
							EditStructure.class);

					intent.putExtra(Keys.SONGNAME, getActivity().getIntent()
							.getStringExtra(Keys.SONGNAME));

					startActivity(intent);
					getActivity().finish();
				}
			});

			ImageView finishedChords = (ImageView) view.findViewById(R.id.ivFinishedChords);
			
			finishedChords.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					saveSong(getActivity().getIntent());
					getActivity().finish();
				}
			});
			
			ImageView goToStrumming = (ImageView) view
					.findViewById(R.id.ivGoToStrumming);

			goToStrumming.setVisibility(View.INVISIBLE);
			goToStrumming.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					// save changes
					
					Intent intent = getActivity().getIntent();
		        	Intent newIntent = new Intent(getActivity(), EditStrumming.class);
		        	newIntent.putExtra(Keys.USERNAME, 
		        			intent.getStringExtra(Keys.USERNAME));
		        	newIntent.putExtra(Keys.ARCHIVENAME, 
		        			intent.getStringExtra(Keys.ARCHIVENAME));
		        	newIntent.putExtra(Keys.SONGNAME, 
		        			intent.getStringExtra(Keys.SONGNAME));
		            startActivity(newIntent);
					getActivity().finish();
				}
			});
		}

		private void highlightWord(EditText etChords, int startPos,
				int cursorPos) {
			Spannable span = new SpannableString(etChords.getText());
			ForegroundColorSpan[] spans = span.getSpans(0, etChords.getText()
					.length(), ForegroundColorSpan.class);
			for (int i = spans.length - 1; i >= 0; i--) {
				if (spans[i].getForegroundColor() == Color.rgb(255, 80, 52)) {
					span.removeSpan(spans[i]);
					break;
				}
			}
			String text = span.toString();
			int endPos = Math.min(Math.max(text.indexOf(' ', startPos), -1),
					Math.max(text.indexOf('\n', startPos), -1));
			if (endPos == -1) {
				endPos = text.length() - 1;
			}
			span.setSpan(new ForegroundColorSpan(Color.rgb(255, 80, 52)), startPos,
					endPos, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
			etChords.setText(span);
			etChords.setSelection(cursorPos);
		}

		private void createChord(String chord) {
			EditText etChords = (EditText) getActivity().findViewById(
					R.id.etChords);
			Spannable tag;
			tag = SpanUtils.setSize(chord, 1f);
			tag = SpanUtils.setColor(tag, Color.BLUE);
			Editable text = etChords.getText();
			// check, if chord exceeds current line width
			Layout layout = etChords.getLayout();
			int cursor = etChords.getSelectionStart();
			int curLine = layout.getLineForOffset(cursor);
			int lineEnd = layout.getLineEnd(curLine);
			int chordEnd = cursor + chord.length() + 1;
			if (chordEnd > lineEnd) {
				String spaces = "";
				for (int i = 0; i < chordEnd - lineEnd; i++) {
					spaces += " ";
				}
				text.insert(lineEnd - 1, spaces);
				lineEnd = layout.getLineEnd(curLine + 1);
				text.insert(lineEnd - 1, spaces);
			}
			text.replace(cursor, cursor + chord.length(), tag);
		}

		private void saveSong(Intent intent) {
			Song song = new Song(getActivity().getIntent().
					getStringExtra(Keys.SONGNAME));
			EditText etChords = 
					(EditText) getActivity().findViewById(R.id.etChords);
			
			String text    = etChords.getText().toString();
			String[] lines = text.split("\n");
			
			String  chordLine = null;
			Section curSec    = null;
			for (String line : lines) {
				if (line.equals("")) {
					//ignore
				}
				else if (line.charAt(0) == '[') {
					if (curSec != null) {
						song.addSection(curSec);								
					}
					curSec = new Section(SectionTypeUtils.sectionTypeFromString(line));
				}
				else if (chordLine == null) {
					chordLine = line;
				}
				else {
					curSec.addLine(new Line(chordLine, line));
					chordLine = null;
				}
			}
			
			song.addSection(curSec);
			Archive archive = Database.getArchive(intent.getStringExtra(Keys.USERNAME), intent.getStringExtra(Keys.ARCHIVENAME));
			archive.addSong(song);
        }
		
		private void setExampleSong(View view) {
			android.widget.EditText chords = (android.widget.EditText) view
					.findViewById(R.id.etChords);

			Intent intent = getActivity().getIntent();
			
			Database database = new Database();
			Song song = database.getSong(intent.getStringExtra(Keys.USERNAME), 
					intent.getStringExtra(Keys.ARCHIVENAME), 
					intent.getStringExtra(Keys.SONGNAME));
			
			chords.setText(song.getText(false));
		}
	}
}
