package de.singalong.layout;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListView;
import android.widget.TextView;
import de.singalong.R;
import de.singalong.data.Archive;
import de.singalong.data.Database;
import de.singalong.data.Song;
import de.singalong.data.User;
import de.singalong.layout.MyActionBarActivity;
import de.singalong.utils.Keys;

public class ImportSong extends MyActionBarActivity {
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_import_song);

        if (savedInstanceState == null) {
            getSupportFragmentManager()
            		.beginTransaction()
                    .add(R.id.flImportSong, new ImportSongFragment())
                    .commit();
        }
    }
	
	
	public static class ImportSongFragment extends Fragment {
		
		DatabaseArrayAdapter songListAdapter;
		
		@Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) 
        {
            View rootView;
            
            rootView = inflater.inflate(
            		R.layout.f_import_song, container, false);
            
            createSongList(rootView);
            return rootView;
        }
		
		private void createSongList(View view) {
			String             userName;
			String             archiveName;
			Archive            archive;
			ListView           songList;
			EditText           searchSong;
			final List<String> databaseSongs;
			List<String>       archiveSongs;
			
			// get user and archive
			userName    = 
					getActivity().getIntent().getStringExtra(Keys.USERNAME);
			archiveName = 
					getActivity().getIntent().getStringExtra(Keys.ARCHIVENAME);
        	archive     = Database.getArchive(userName, archiveName);
        	
        	// get view elements
			songList   = (ListView) view.findViewById(R.id.lvDatabaseSongs);
			searchSong =
					(EditText) view.findViewById(R.id.tvSearchDatabaseSongs);
			
			// get song lists
			databaseSongs   = Database.getDatabaseSongNames();
			archiveSongs    = archive.getAllSongNames();
			songListAdapter = 
    				new DatabaseArrayAdapter(
    						getActivity(),
    						android.R.layout.simple_list_item_1,
    						databaseSongs,
    						archiveSongs);
			
			songList.setAdapter(songListAdapter);
			
			songList.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(
						AdapterView<?> parent, View view, int position, long id) 
				{
					ImportSongDialogFragment dialog;
					String                   songName;
					List<String>             filteredSongs;
					
					filteredSongs = songListAdapter.getFilteredSongs();
					songName      = filteredSongs.get(position);
					dialog        = new ImportSongDialogFragment();
					
					dialog.setSongName(songName);
					dialog.show(
							getActivity().getSupportFragmentManager(), 
							"ImportSongDialogFragment");
				}
				
			});
			
			searchSong.addTextChangedListener(new TextWatcher() {

				@Override
				public void afterTextChanged(Editable searchString) {
					ImportSongFragment.this.songListAdapter
						.getFilter().filter(searchString);
				}

				@Override
				public void beforeTextChanged(CharSequence arg0, int arg1,
						int arg2, int arg3) 
				{
					
				}

				@Override
				public void onTextChanged(CharSequence arg0, int arg1,
						int arg2, int arg3) 
				{
					
				}
				
			});
		}
	}
	
	public static class DatabaseArrayAdapter 
		extends ArrayAdapter<String>
		implements Filterable
	{
		
		private List<String>     databaseSongs;
		private List<String>     archiveSongs;
		private List<String>     filteredSongs;
		private FragmentActivity activity;
		private ItemFilter       searchFilter;
		
		public DatabaseArrayAdapter(
				FragmentActivity _activity, 
				int              _resource, 
				List<String>     _databaseSongs,
				List<String>     _archiveSongs) 
		{
			super(_activity, _resource, _databaseSongs);
			
			activity      = _activity;
			databaseSongs = new ArrayList<String>(_databaseSongs);
			filteredSongs = _databaseSongs;
			archiveSongs  = _archiveSongs;
			searchFilter  = new ItemFilter();
		}
		
		public List<String> getFilteredSongs() {
			return filteredSongs;
		}
		
		@Override
		public int getCount() {
			return filteredSongs.size();
		}
		
		@Override
		public boolean isEnabled(int position) {
			return !archiveSongs.contains(filteredSongs.get(position));
		}
		
		public int myGetItemId(int position) {
			if (isEnabled(position)) {
				return R.id.listItemActivated;
			}
			else {
				return R.id.listItemDeactivated;
			}
		}
		
		@Override
		public String getItem(int position) {
			return filteredSongs.get(position);
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View           v;
			LayoutInflater vi;
			TextView       tv;
			
			// TODO: use ViewHolder?
			
			v  = convertView;
	        vi = activity.getLayoutInflater();
	        
	        if (isEnabled(position)) {
	        	v = vi.inflate(R.layout.list_item_activated, null);	        	
	        }
	        else {
	        	v = vi.inflate(R.layout.list_item_deactivated, null);
	        }		       
			
			tv = (TextView) v.findViewById(myGetItemId(position));
			tv.setText(getItem(position));
			
		    return v;
		}
		
		@Override
		public Filter getFilter() {
			return searchFilter;
		}
		
		private class ItemFilter extends Filter {
	        @Override
	        protected FilterResults performFiltering(CharSequence constraint) {

	            String                  searchString; 
	            String                  songName;
	            FilterResults           results;
	            int                     count;
	            final ArrayList<String> filteredList;
	            
	            searchString = constraint.toString().toLowerCase();
	            results      = new FilterResults();
	            count        = databaseSongs.size();
	            filteredList = new ArrayList<String>(count);

	            for (int i = 0; i < count; ++i) {
	                songName = databaseSongs.get(i);
	                if (songName.toLowerCase().contains(searchString)) {
	                    filteredList.add(songName);
	                }
	            }

	            results.values = filteredList;
	            results.count  = filteredList.size();

	            return results;
	        }

	        @SuppressWarnings("unchecked")
	        @Override
	        protected void publishResults(CharSequence constraint, FilterResults results) {
	        	filteredSongs = (ArrayList<String>) results.values;
	            
            	notifyDataSetChanged();
	        }

	    }
	}
	
	public static class ImportSongDialogFragment extends DialogFragment {
    	
		private String songName;
		
		public void setSongName(String _songName) {
			songName = _songName;
		}
		
    	@Override
    	public Dialog onCreateDialog(Bundle savedInstancesState) {
            Intent              intent;
            final String        archiveName;
            StringBuilder       title;
            AlertDialog.Builder builder;
            
            intent      = getActivity().getIntent();
			archiveName = intent.getStringExtra(Keys.ARCHIVENAME);
			title       = new StringBuilder();
            builder     = new AlertDialog.Builder(getActivity());
            
            title.append(songName)
                 .append(" zu ")
                 .append(archiveName)
                 .append(" hinzufügen?");
            
            builder.setTitle(title.toString())
                   .setPositiveButton(R.string.add, 
                		   new DialogInterface.OnClickListener() {
		                       public void onClick(
		                    		   DialogInterface dialog, int id) 
		                       {
		                    	   String  userName;
		                    	   User    user;
		                    	   Archive archive;
		                    	   Song    song;
		                    	   
		                    	   userName = getActivity().getIntent()
		                    			   .getStringExtra(Keys.USERNAME);
		                    	   
		                    	   // add song to archive
		                    	   user    = Database.getUserData(userName);
		                    	   archive = user.getArchive(archiveName);
		                    	   song    = Database.getDatabaseSong(songName);
		                    	   
		                    	   archive.addSong(song);
		                    	   
		                    	   // go back to song overview
								   getActivity().finish();
		                    	   dialog.dismiss();
		                       }
                   		   })
                   .setNegativeButton(R.string.cancel, 
                		   new DialogInterface.OnClickListener() {
		                       public void onClick(
		                    		   DialogInterface dialog, int id) 
		                       {
		                           dialog.cancel();
		                       }
                   		   });
            
            return builder.create();
    	}
	}
}
