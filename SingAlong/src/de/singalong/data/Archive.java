package de.singalong.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Class to manage an archive.
 * Provides access to all songs in this archive.
 */
public class Archive {
	
	private String            archiveName;
	private Map<String, Song> songs;
	
	public Archive(String _archiveName) {
		archiveName = _archiveName;
		songs       = new HashMap<String, Song>();
	}
	
	public String getName() {
		return archiveName;
	}
	
	/**
	 * Get the names of all songs in this archive.
	 * The resulting array is sorted alphabetically.
	 * 
	 * @return sorted array of song names
	 */
	public List<String> getAllSongNames() {
		Set<String>  songNames;
		List<String> songNamesList;
		
		songNames     = songs.keySet();
		songNamesList = new ArrayList<String>(songNames);
		
		Collections.sort(songNamesList);
		
		return songNamesList;
	}
	
	public Song getSong(String songName) {
		return songs.get(songName);
	}
	
	public void addSong(Song extSong) {
		// TODO: only add clone -> copy Song
		songs.put(extSong.getName(), extSong);
	}
}
