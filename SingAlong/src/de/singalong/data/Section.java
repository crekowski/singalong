package de.singalong.data;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import de.singalong.utils.SpanUtils;
import de.singalong.utils.SectionType;
import de.singalong.utils.SectionTypeUtils;

/**
 * Class representing a section of a song.
 * It has a {@link SectionType} and consists of muliple lines.
 */
public class Section {
	
	private SectionType      type;
	private LinkedList<Line> lines;
	private Set<String>      chords;
	
	public Section(SectionType _type) {
		type   = _type;
		lines  = new LinkedList<Line>();
		chords = new HashSet<String>();
	}
	
	public SectionType getType() {
		return type;
	}
	
	public Set<String> getChords() {
		return chords;
	}
	
	public Spannable getText(boolean textOnly) {
		SpannableStringBuilder text;
		Spannable              typeSpan;
		
		text     = new SpannableStringBuilder();
		typeSpan = SpanUtils.setSize(
				SectionTypeUtils.sectionTypeName(type), 
				1.2f);
		typeSpan = SpanUtils.setColor(typeSpan, Color.GRAY);
		
		text.append(typeSpan);
		text.append("\n");
		
		for (Line line : lines) {
			text.append(line.getText(textOnly));
		}
		
		text.append("\n");
		
		return text;
	}
	
	/**
	 * Text, containing chords and song text, is given as a string. Lines are
	 * separated by the standard line separator "\n".
	 * Chords and text are splitted and transferred into lines.
	 * 
	 * @param rawText
	 */
	public void createFromRawText(String rawText) {
		String[] textParts;
		
		textParts = rawText.split("\n");
		
		for (int i = 0; i < textParts.length-1; i += 2) {
			this.addLine(new Line(textParts[i], textParts[i+1]));
		}
	}
	
	public void addLine(Line l) {
		lines.add(l);
		chords.addAll(l.getChords());
	}
	
	public void transposeUp() {
		for (Line l : lines) {
			l.transposeUp();
		}
	}
	
	public void transposeDown() {
		for (Line l : lines) {
			l.transposeDown();
		}
	}
}
