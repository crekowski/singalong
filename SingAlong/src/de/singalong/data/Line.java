package de.singalong.data;

import java.util.HashSet;
import java.util.LinkedList;

import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableStringBuilder;

import de.singalong.utils.ChordUtils;
import de.singalong.utils.SpanUtils;

/**
 * Class representing a line of a song.
 * Each line has one or both of text and chords.
 */
public class Line {
	
	private LinkedList<ChordWord> line;
	
	public Line() {
		line = new LinkedList<ChordWord>();
	}
	
	public Line(String words) {
		line = new LinkedList<ChordWord>();
		
		words.replace("\n", "");
		String[] w = words.split(" ");
		
		for (String word : w) {
			line.add(new ChordWord("", word));
		}
	}
	
	public Line(String chords, String words) {
		line = new LinkedList<ChordWord>();
		
		String   chord;
		String[] w = words.split(" ");
		
		for (String word : w) {
			chord = chords.substring(0, Math.min(chords.length(), word.length())).replace(" ", "");
			line.add(new ChordWord(chord, word));
			chords = chords.substring(Math.min(word.length() + 1, chords.length()), chords.length());
		}
	}
	
	public void addChrodWord(ChordWord cw) {
		line.add(cw);
	}
	
	/**
	 * @param  textOnly true if chords should not be displayed
	 * @return String containing the chords of the line concatenated with the
	 *         words of the line
	 */
	public Spannable getText(boolean textOnly) {
		String   chord;
		String   word;		
		String chords = "";
		String words = "";
		
		if (!textOnly) {
			for (ChordWord cw : line) {
				chord = cw.getChord();
				word  = cw.getWord();
				
				// adjust length with spaces
				while (word.length() < chord.length()) {
					word += " ";
				}
				while (word.length() > chord.length()) {
					chord += " ";
				}
				
				chords += chord + " ";
				words += word  + " ";
			}
			
			// replace last " " with "\n"
			chords = chords.substring(0, chords.length() - 1) + "\n";
			words = words.substring(0, words.length() - 1) + "\n";		
		}
		else {
			for (ChordWord cw : line) {
				words += cw.getWord() + " ";
			}
			// replace last " " with "\n"
			words = words.substring(0, words.length() - 1) + "\n";
		}	
		
		SpannableStringBuilder text = new SpannableStringBuilder();
		text.append(SpanUtils.setColor(chords, Color.BLUE));
		text.append(words);
		
		return text;
	}
	
	public HashSet<String> getChords() {
		HashSet<String> chords = new HashSet<String>();
		for (ChordWord cw : line) {
			chords.add(cw.getChord());
		}
		return chords;
	}
	
	public void transposeUp() {
		for (ChordWord cw : line) {
			cw.setChord(ChordUtils.transposeUp(cw.getChord()));			
		}
	}
	
	public void transposeDown() {
		for (ChordWord cw : line) {
			cw.setChord(ChordUtils.transposeDown(cw.getChord()));			
		}
	}
/*
	private String chords;
	private String songText;
	
	public Line(String _chords, String _text) {
		chords     = _chords;
		songText   = _text;
	}
	
	public List<String> getChords() {
		// TODO: what if there are multiple spaces in a row?
		return Arrays.asList(chords.split(" "));
	}
	
	public String getSongText() {
		return songText;
	}
	
	public Spannable getText() {
		SpannableStringBuilder text = new SpannableStringBuilder();
		
		text.append(SpanUtils.setColor(chords, Color.BLUE));
		text.append(songText);
		
		return text;
	}
	
	public void transposeUp() {
		String[]      chordsArray;
		StringBuilder newChords;
		
		chordsArray = chords.split(" ");
		newChords   = new StringBuilder();
		
		for (String chord : chordsArray) {
			if (chord.equals("\n")) {
				newChords.append(chord);
			}
			else if (chord.length() > 0) {
				newChords.append(ChordUtils.transposeUp(chord));
			}
			newChords.append(" ");
			// TODO: regard changed spaces
		}
		
		chords = newChords.toString();
	}
	
	public void transposeDown() {
		String[]      chordsArray;
		StringBuilder newChords;
		
		chordsArray = chords.split(" ");
		newChords   = new StringBuilder();
		
		for (String chord : chordsArray) {
			if (chord.equals("\n")) {
				newChords.append(chord);
			}
			else if (chord.length() > 0) {
				newChords.append(ChordUtils.transposeDown(chord));
			}
			newChords.append(" ");
			// TODO: regard changed spaces
		}
		
		chords = newChords.toString();
	}
	*/
}
