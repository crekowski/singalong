package de.singalong.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.singalong.data.songs.Anaconda;
import de.singalong.data.songs.Atemlos;
import de.singalong.data.songs.BoomBoomBoomBoom;
import de.singalong.data.songs.ILikeBigButts;
import de.singalong.data.songs.VivaLaVida;

public class Database {
	
	private static Map<String, User> users;
	private static Map<String, Song> databaseSongs;
	private static User              marianne;
	private static Archive           timsArchive;
	private static Song              songBoom;
	private static Song              songViva;
	private static Song              songAtemlos;
	private static Song              songAnaconda;
	private static Song              songBigButts;
	
	
	/*
	 * users
	 */
	public static Set<String> getAllUserNames() {
		return users.keySet();
	}
	
	public static User getUserData(String userName) {
		User user;
		
		if (users == null) {
			// initialize users
			users = new HashMap<String, User>();
			users.put("Marianne", exampleUserMarianne());
		}
		
		user = users.get(userName);
		
		if (user == null) {
			// new user created
			user = new User(userName);
			users.put(userName, user);
		}
		
		return user;
	}
	
	private static User exampleUserMarianne() {
		if (marianne == null) {
			Archive mArchive;
			
			marianne = new User("Marianne");
			
			// create archive
			mArchive = new Archive("Lieblingssongs :-)");
			fillMariannesArchive(mArchive);
			
			// add archives
			marianne.addArchive(mArchive);
			marianne.addArchive(timsExampleArchive());
		}
		
		return marianne;
	}
	
	
	/*
	 * archives
	 */
	
	public static Archive getArchive(String userName, String archiveName) {
		User user;
		
		user = getUserData(userName);
		
		return user.getArchive(archiveName);
	}
	
	private static void fillMariannesArchive(Archive mArchive) {
		mArchive.addSong(boomBoomBoomBoom());
		mArchive.addSong(vivaLaVida());
	}
	
	private static Archive timsExampleArchive() {
		if (timsArchive == null) {
			timsArchive = new Archive("Tims Trash-Party");
			
			timsArchive.addSong(atemlos());
			timsArchive.addSong(anaconda());
			timsArchive.addSong(iLikeBigButts());
		}
		
		return timsArchive;
	}
	
	
	/*
	 * songs
	 */
	
	public static List<String> getDatabaseSongNames() {
		// TODO: add newly created songs
		
		Set<String>  songNames;
		List<String> songNamesList;
		
		if (databaseSongs == null) {
			databaseSongs = new HashMap<String, Song>();
			
			databaseSongs.put(boomBoomBoomBoom().getName(), boomBoomBoomBoom());
			databaseSongs.put(vivaLaVida().getName(), vivaLaVida());
			databaseSongs.put(atemlos().getName(), atemlos());
			databaseSongs.put(anaconda().getName(), anaconda());
			databaseSongs.put(iLikeBigButts().getName(), iLikeBigButts());
		}
		
		songNames = databaseSongs.keySet();
		songNamesList = new ArrayList<String>(songNames);
		
		Collections.sort(songNamesList);
		
		return songNamesList;
	}
	
	public static Song getDatabaseSong(String songName) {
		return databaseSongs.get(songName);
	}
	
	public static Song getSong(
			String userName, String archiveName, String songName) 
	{
		Archive archive;
		
		archive = getArchive(userName, archiveName);
		
		return archive.getSong(songName);
	}
	
	private static Song boomBoomBoomBoom() {
		if (songBoom == null) { 
			songBoom = new BoomBoomBoomBoom();
		}
		
		return songBoom;
	}
	
	private static Song vivaLaVida() {
		if (songViva == null) {
			songViva = new VivaLaVida();
		}
		
		return songViva;
	}
	
	private static Song atemlos() {
		if (songAtemlos == null) {
			songAtemlos = new Atemlos();
		}
		
		return songAtemlos;
	}
	
	private static Song anaconda() {
		if (songAnaconda == null) {
			songAnaconda = new Anaconda();
		}
		
		return songAnaconda;
	}
	
	private static Song iLikeBigButts() {
		if (songBigButts == null) {
			songBigButts = new ILikeBigButts();
		}
		
		return songBigButts;
	}
}
