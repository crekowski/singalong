package de.singalong.data;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import android.text.Spannable;
import android.text.SpannableStringBuilder;

/**
 * Class representing a song.
 * A song consists of multiple section (for example verse, chorus).
 * Also it can have information on the used chords and the strumming pattern.
 */
public class Song {
	
	private String        songName;
	private List<Section> sections;
	private Set<String>   chords;
	private String        strumming;
	
	public Song(String _songName) {
		songName  = _songName;
		sections  = new LinkedList<Section>();
		chords    = new HashSet<String>();
		strumming = null;
	}
	
	public String getName() {
		return songName;
	}
	
	public Set<String> getChords() {
		return chords;
	}
	
	public String getStrumming() {
		return strumming;
	}
	
	public Spannable getText(boolean textOnly) {
		SpannableStringBuilder text;
		
		text = new SpannableStringBuilder();
		
		for (Section sec : sections) {
			text.append(sec.getText(textOnly));
		}
		
		return text;
	}
	
	public void addSection(Section sec) {
		sections.add(sec);
		chords.addAll(sec.getChords());
	}
	
	public void setStrumming(String str) {
		strumming = str;
	}
	
	public void transposeUp() {
		for (Section sec : sections) {
			sec.transposeUp();
		}
	}
	
	public void transposeDown() {
		for (Section sec : sections) {
			sec.transposeDown();
		}
	}
}
