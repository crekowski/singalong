package de.singalong.data;

/**
 * Class representing one word of a line with its corresponding chord.
 * Chord might be empty ("").
 */
public class ChordWord {
	
	String chord;
	String word;
	
	public ChordWord(String chord, String word) {
		this.chord = chord;
		this.word  = word;
	}
	
	public String getChord() {
		return chord;
	}
	
	public String getWord() {
		return word;
	}
	
	public void setChord(String chord) {
		this.chord = chord;
	}
	
	public void setWord(String word) {
		this.word = word;
	}
}
