package de.singalong.data.songs;

import de.singalong.data.Section;
import de.singalong.data.Song;
import de.singalong.utils.SectionType;

public class ILikeBigButts extends Song {
	
	public ILikeBigButts() {
		super("I like big butts");
		
		addSection(intro());
		addSection(verse1());
		addSection(preChorus());
		addSection(chorus());
		addSection(interlude());
	}
	
	private Section intro() {
		Section intro;
		String  introText;
		
		intro     = new Section(SectionType.INTRO);
		introText = 
				"G       D       Em\n" +
				"Whoa oh whoa oh\n" +
				"C       D       G\n" +
				"Whoa oh whoa oh, Vengaboys are back in town\n" +
				"G       D       Em\n" +
				"Whoa oh whoa oh\n" +
				"C       D       G\n" +
				"Whoa oh whoa oh\n" +
				"G       D       Em\n" +
				"Whoa oh whoa oh\n" +
				"C       D       G\n" +
				"Whoa oh whoa oh\n";
		intro.createFromRawText(introText);
		
		return intro;
	}
	
	private Section verse1() {
		Section verse1;
		String  verse1Text;
		
		verse1     = new Section(SectionType.VERSE);
		verse1Text = 
				"G          D                   Em\n" +
				"If you're alone and you need a friend\n" +
				"C          D        G\n" +
				"Someone to make you forget your problems\n" +
				"G         D\n" +
				"Just come along baby\n" +
				"Em\n" +
				"Take my hand\n" +
				"C            D        G\n" +
				"I'll be your lover tonight\n";
		verse1.createFromRawText(verse1Text);
		
		return verse1;
	}
	
	private Section preChorus() {
		Section preChorus;
		String  preChorusText;
		
		preChorus     = new Section(SectionType.PRE_CHORUS);
		preChorusText =
				"\n" +
				"Whoa oh whoa oh\n" +
				"\n" +
				"This is what I wanna do\n" +
				"\n" +
				"Whoa oh whoa oh\n" +
				"\n" +
				"Let's have some fun\n" +
				"\n" +
				"Whoa oh whoa oh\n" +
				"\n" +
				"One on one just me and you\n" +
				"\n" +
				"Whoa oh whoa oh\n";
		preChorus.createFromRawText(preChorusText);
		
		return preChorus;
	}
		
	private Section chorus() {
		Section chorus;
		String  chorusText;
		
		chorus     = new Section(SectionType.CHORUS);
		chorusText =
				"\n" +
				"Boom boom boom boom\n" +
				"\n" +
				"I want you in my room\n" +
				"\n" +
				"Let's spend the night together\n" +
				"\n" +
				"From now until forever\n" +
				"\n" +
				"Boom boom boom boom\n" +
				"\n" +
				"I wanna double boom\n" +
				"\n" +
				"Let's spend the night together\n" +
				"\n" +
				"Together in my room\n";
		chorus.createFromRawText(chorusText);
		
		return chorus;
	}
	
	private Section interlude() {
		Section interlude;
		String  interludeText;
		
		interlude     = new Section(SectionType.INTERLUDE);
		interludeText =
				"\n" +
				"Whoa oh whoa oh\n" +
				"\n" +
				"Everybody get on down\n" +
				"\n" +
				"Whoa oh whoa oh\n" +
				"\n" +
				"Vengaboys are back in town\n";
		interlude.createFromRawText(interludeText);
		
		return interlude;
	}
}
