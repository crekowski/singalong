package de.singalong.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Class to manage a user.
 * Provides access to the users own and external archives.
 */
public class User {
	
	private String               userName;
	private Map<String, Archive> archives;
	
	public User(String _userName) {
		userName = _userName;
		archives = new HashMap<String, Archive>();
	}
	
	public String getName() {
		return userName;
	}
	
	/**
	 * Get the names of all archives of this user.
	 * The resulting list is sorted alphabetically.
	 * 
	 * @return sorted list of archive names
	 */
	public List<String> getAllArchiveNames() {
		Set<String>  archiveNames;
		List<String> archiveNamesList;
		
		archiveNames     = archives.keySet();		
		archiveNamesList = new ArrayList<String>(archiveNames);
		
		Collections.sort(archiveNamesList);
		
		return archiveNamesList;
	}
	
	public Archive getArchive(String archiveName) {
		return archives.get(archiveName);
	}
	
	public void newArchive(String archiveName) {
		Archive newArchive = new Archive(archiveName);
		
		addArchive(newArchive);
	}
	
	public void addArchive(Archive extArchive) {
		archives.put(extArchive.getName(), extArchive);
	}
	
	public void removeArchive(String archiveName) {
		archives.remove(archiveName);
	}
}
