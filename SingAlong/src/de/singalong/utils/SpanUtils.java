package de.singalong.utils;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;

public class SpanUtils {
	
	public static Spannable setColor(String text, int color) {
		Spannable span = new SpannableString(text);
		
		span.setSpan(
				new ForegroundColorSpan(color), 
				0, 
				text.length(), 
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		
		return span;
	}
	
	public static Spannable setColor(Spannable text, int color) {
		text.setSpan(
				new ForegroundColorSpan(color), 
				0, 
				text.length(), 
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		
		return text;
	}
	
	public static Spannable setSize(String text, float size) {
		Spannable span = new SpannableString(text);
		
		span.setSpan(
				new RelativeSizeSpan(size),
				0, 
				text.length(), 
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		
		return span;
	}
	
	public static Spannable setSize(Spannable text, float size) {
		text.setSpan(
				new RelativeSizeSpan(size),
				0, 
				text.length(), 
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		
		return text;
	}
}
