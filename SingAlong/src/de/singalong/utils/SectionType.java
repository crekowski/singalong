package de.singalong.utils;

public enum SectionType {
	INTRO,
	VERSE,
	PRE_CHORUS,
	CHORUS,
	BRIDGE,
	INTERLUDE,
	OUTRO,
	OTHER
}
