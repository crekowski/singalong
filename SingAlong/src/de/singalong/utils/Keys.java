package de.singalong.utils;

public interface Keys {
	public static final String USERNAME          = "de.singalong.USER";
	public static final String INVITING_USERNAME = "de.singalong.INVITING_USER";
	public static final String ARCHIVENAME       = "de.singalong.ARCHIVENAME";
	public static final String SONGNAME          = "de.singalong.SONGNAME";
	public static final String SONGTEXT          = "de.singalong.SONGTEXT";
}
