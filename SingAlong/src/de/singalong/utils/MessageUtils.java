package de.singalong.utils;

public class MessageUtils {
	
	/**
	 * Checks if the given name is valid concerning length and leading/trailing
	 * spaces.
	 * 
	 * @param name
	 * @return validity of the given name
	 */
	public static boolean checkName(String name)
	{
		int len;
		
		len = name.length();
		
		return !
			(len == 0 ||               // empty
			 len > 20 ||               // too long
			 name.matches("\\s+.*") || // starts with spaces
			 name.matches(".*\\s+"));  // ends with spaces
	}
}
