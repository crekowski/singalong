package de.singalong.utils;

public class ChordUtils {
	
	public enum Tone {
		C,
		Cis,
		Db,
		D,
		Dis,
		Eb,
		E,
		Eis,
		Fb,
		F,
		Fis,
		Gb,
		G,
		Gis,
		Ab,
		A,
		Ais,
		Bb,
		B,
		Bis,
		Cb
	}
	
	public static String transposeUp(String chordString) {
		Chord chord;
		
		chord = new Chord(chordString);
		chord.transposeUp();
		
		return chord.toString();
	}
	
	public static String transposeDown(String chordString) {
		Chord chord;
		
		chord = new Chord(chordString);
		chord.transposeDown();
		
		return chord.toString();
	}
	
	public static Tone simplify(Tone tone) {
		// TODO: where to use?
		switch (tone) {
		case Bis:
			return Tone.C;
		case Cb:
			return Tone.B;
		case Eis:
			return Tone.F;
		case Fb:
			return Tone.E;
		case Ais:
			// not necessarily
//			return Tone.B;
		default:
			return tone;
		}
	}
	
	private static String toneToString(Tone tone) {
		switch (tone) {
		case A:
			return "A";
		case Ab:
			return "Ab";
		case Ais:
			return "A#";
		case B:
			return "B";
		case Bb:
			return "Bb";
		case Bis:
			return "B#";
		case C:
			return "C";
		case Cb:
			return "Cb";
		case Cis:
			return "C#";
		case D:
			return "D";
		case Db:
			return "Db";
		case Dis:
			return "D#";
		case E:
			return "E";
		case Eb:
			return "Eb";
		case Eis:
			return "E#";
		case F:
			return "F";
		case Fb:
			return "Fb";
		case Fis:
			return "F#";
		case G:
			return "G";
		case Gb:
			return "Gb";
		case Gis:
			return "G#";
		default:
			return null;
		}
	}
	
	private static Tone stringToTone(String tone) {
		if (tone.equals("A")) {
			return Tone.A;
		}
		if (tone.equals("Ab")) {
			return Tone.Ab;
		}
		if (tone.equals("A#")) {
			return Tone.Ais;
		}
		if (tone.equals("B")) {
			return Tone.B;
		}
		if (tone.equals("Bb")) {
			return Tone.Bb;
		}
		if (tone.equals("B#")) {
			return Tone.Bis;
		}
		if (tone.equals("C")) {
			return Tone.C;
		}
		if (tone.equals("Cb")) {
			return Tone.Cb;
		}
		if (tone.equals("C#")) {
			return Tone.Cis;
		}
		if (tone.equals("D")) {
			return Tone.D;
		}
		if (tone.equals("Db")) {
			return Tone.Db;
		}
		if (tone.equals("D#")) {
			return Tone.Dis;
		}
		if (tone.equals("E")) {
			return Tone.E;
		}
		if (tone.equals("Eb")) {
			return Tone.Eb;
		}
		if (tone.equals("E#")) {
			return Tone.Eis;
		}
		if (tone.equals("F")) {
			return Tone.F;
		}
		if (tone.equals("Fb")) {
			return Tone.Fb;
		}
		if (tone.equals("F#")) {
			return Tone.Fis;
		}
		if (tone.equals("G")) {
			return Tone.G;
		}
		if (tone.equals("Gb")) {
			return Tone.Gb;
		}
		if (tone.equals("G#")) {
			return Tone.Gis;
		}
		return null;
	}
	
	private static Tone stepUp(Tone tone) {
		switch (tone) {
		case A:
			return Tone.Ais;
		case Ab:
			return Tone.A;
		case Ais:
			return Tone.B;
		case B:
			return Tone.C;
		case Bb:
			return Tone.B;
		case Bis:
			return Tone.Cis;
		case C:
			return Tone.Cis;
		case Cb:
			return Tone.C;
		case Cis:
			return Tone.D;
		case D:
			return Tone.Dis;
		case Db:
			return Tone.D;
		case Dis:
			return Tone.E;
		case E:
			return Tone.F;
		case Eb:
			return Tone.E;
		case Eis:
			return Tone.Fis;
		case F:
			return Tone.Fis;
		case Fb:
			return Tone.F;
		case Fis:
			return Tone.G;
		case G:
			return Tone.Gis;
		case Gb:
			return Tone.G;
		case Gis:
			return Tone.A;
		default:
			return null;
		}
	}
	
	private static Tone stepDown(Tone tone) {
		switch (tone) {
		case A:
			return Tone.Gis;
		case Ab:
			return Tone.G;
		case Ais:
			return Tone.A;
		case B:
			return Tone.Ais;
		case Bb:
			return Tone.A;
		case Bis:
			return Tone.B;
		case C:
			return Tone.B;
		case Cb:
			return Tone.Ais;
		case Cis:
			return Tone.C;
		case D:
			return Tone.Cis;
		case Db:
			return Tone.C;
		case Dis:
			return Tone.D;
		case E:
			return Tone.Dis;
		case Eb:
			return Tone.D;
		case Eis:
			return Tone.E;
		case F:
			return Tone.E;
		case Fb:
			return Tone.Dis;
		case Fis:
			return Tone.F;
		case G:
			return Tone.Fis;
		case Gb:
			return Tone.F;
		case Gis:
			return Tone.G;
		default:
			return null;
		}
	}
	
	
	private static class Chord {
		private Tone   tone;
		private String extension;
		
		public Chord (String chord) {
			if (chord.length() == 1) {
				// plain chord
				tone      = ChordUtils.stringToTone(chord);
				extension = "";
			}
			else {
				char accidental = chord.charAt(1);
				
				if (accidental == 'b' || accidental == '#') {
					// chord with accidental
					tone      = ChordUtils.stringToTone(chord.substring(0, 2));
					extension = chord.substring(2);
				}
				else {
					// there is no accidental, but an extension
					tone      = ChordUtils.stringToTone(chord.substring(0, 1));
					extension = chord.substring(1);
				}
			}
		}
		
		public void transposeUp() {
			tone = ChordUtils.stepUp(tone);
		}
		
		public void transposeDown() {
			tone = ChordUtils.stepDown(tone);
		}
		
		public String toString() {
			return ChordUtils.toneToString(tone) + extension;
		}
	}
}
