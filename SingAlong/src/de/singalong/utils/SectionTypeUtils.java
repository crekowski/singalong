package de.singalong.utils;

public class SectionTypeUtils {
	
	public static String sectionTypeName(SectionType type) {
		switch(type) {
		case INTRO:      return "[Intro]";
		case VERSE:      return "[Strophe]";
		case PRE_CHORUS: return "[Prechorus]";
		case CHORUS:     return "[Refrain]";
		case BRIDGE:     return "[Bridge]";
		case INTERLUDE:  return "[Interlude]";
		case OUTRO:      return "[Outro]";
		case OTHER:      return ""; //"[Andere]";
		default: return "";
		}
	}
	
	public static SectionType sectionTypeFromString(String s) {
		if (s.equals("[Intro]")) {
			return SectionType.INTRO;
		}
		else if (s.equals("[Strophe]")) {
			return SectionType.VERSE;
		} 
		else if (s.equals("[Prechorus]")) {
			return SectionType.PRE_CHORUS;
		} 
		else if (s.equals("[Refrain]")) {
			return SectionType.CHORUS;
		} 
		else if (s.equals("[Bridge]")) {
			return SectionType.BRIDGE;
		} 
		else if (s.equals("[Interlude]")) {
			return SectionType.INTERLUDE;
		} 
		else if (s.equals("[Outro]")) {
			return SectionType.OUTRO;
		} 
		else {
			return SectionType.OTHER;
		} 
	}
}
